
LINUX="https://www.kernel.org/pub/linux/kernel/v3.x/linux-3.8.8.tar.xz"
OPENV="http://openvswitch.org/releases/openvswitch-1.10.0.tar.gz"

curl -L $LINUX | tar -xJv
curl -L $OPENV | tar -xzv
