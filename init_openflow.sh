
CONTROLLER=$1
shift
OVS_BRIDGE=$1
shift
CONTROLLER_PORT="6633"
OVS_PORTS=$@

# Add the bridge 
ovs-vsctl add-br $OVS_BRIDGE

# Add the ports
for port in $OVS_PORTS; do
    # Make sure the port is up before adding it
    ifconfig $port up
    ovs-vsctl add-port $OVS_BRIDGE $port
done

# Set the controller
ovs-vsctl set-controller $OVS_BRIDGE "tcp:$CONTROLLER:$CONTROLLER_PORT"
