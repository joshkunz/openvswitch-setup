#!/usr/bin/env bash

OVS_DB="/usr/local/etc/openvswitch/conf.db"
OVSDB_SOCK="/usr/local/var/run/openvswitch/db.sock"

# A script to start open vswtich on a linux box in

CREATED=0

# Make sure we've loaded the kernel modules
modprobe openvswitch
modprobe bridge

# If the db doesn't exist create it
if test ! -f $OVS_DB; then
	echo "Creating OpenVSwtich database..."
	ovsdb-tool create $OVS_DB
	CREATED=1
fi

echo "Starting OpenVSwtich database controller..."
ovsdb-server $OVS_DB --remote="punix:$OVSDB_SOCK" --pidfile --detach

# If we created a new database, initalize it first
if test $CREATED -eq "1"; then
	echo "Initializing OpenVSwtich database..."
	ovs-vsctl --db="unix:$OVSDB_SOCK" --no-wait init
fi

echo "Starting OpenVSwitch bridge daemon..."
ovs-vswitchd unix:$OVSDB_SOCK --detach --pidfile
