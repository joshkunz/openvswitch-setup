#!/usr/bin/env bash
ps aux | grep ovs | awk '{ print $2; }' | tr '\n' '\0' | xargs -0 kill
