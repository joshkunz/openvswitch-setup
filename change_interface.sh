IP_ADDR=$1
NETMASK=$2
DEFAULT_GATEWAY=$3

INTERFACE="br0"
OF_INTERFACE="eth0"

cat <<EOF
IP Address: $IP_ADDR
Netmask: $NETMASK
Default Gateway: $DEFAULT_GATEWAY
Press enter to apply the configuration...
EOF

# Wait for input
read X

# Zero out the interface
ifconfig $INTERFACE 0

# Set the ip address to the one specified
ifconfig $OF_INTERFACE $IP_ADDR netmask $NETMASK

# Change the default route over
route add default gw $DEFAULT_GATEWAY $OF_INTERFACE
route del default gw $DEFAULT_GATEWAY $INTERFACE

echo "All done..."
